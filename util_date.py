import datetime

DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
DATE_FORMAT = "%Y-%m-%d"
TIME_FORMAT = "%H:%M:%S"
SIMPLE_DATE_TIME_FMT = '%m-%d %H:%M'

#距离明天0点的秒数
def time_delta():
    now = datetime.datetime.now()
    tomorrow = datetime.datetime(now.year, now.month, now.day) + datetime.timedelta(days=1)
    delta = tomorrow - now
    seconds = delta.total_seconds()
    return seconds
