# 导入所需的库
from Crypto.Cipher import AES
import base64


# 定义一个加密类
class AESCipher:
    def __init__(self, key, iv):
        # 设置密钥和初始向量
        self.key = key.encode('utf-8')
        self.iv = iv.encode('utf-8')

    def pkcs7padding(self, text):
        # 使用PKCS7方式进行填充，确保明文长度为16字节的倍数
        text_length = len(text)
        bytes_length = len(text.encode('utf-8'))
        padding_size = text_length if bytes_length == text_length else bytes_length
        padding = 16 - padding_size % 16
        padding_text = chr(padding) * padding
        return text + padding_text

    def aes_encrypt(self, content):
        # 使用AES加密
        cipher = AES.new(self.key, AES.MODE_CBC, self.iv)
        # 对明文进行填充
        content_padding = self.pkcs7padding(content)
        # 加密并编码
        encrypt_bytes = cipher.encrypt(content_padding.encode('utf-8'))
        result = base64.b64encode(encrypt_bytes).decode('utf-8')
        return result
