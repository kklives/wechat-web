import pyodbc
def get_conn():
    SERVER = '172.16.31.1,9999'
    DATABASE = 'znykt_Auto'
    USERNAME = 'sa'
    PASSWORD = '123'
    connectionString = f'DRIVER={{ODBC Driver 18 for SQL Server}};SERVER={SERVER};DATABASE={DATABASE};UID={USERNAME};PWD={PASSWORD};TrustServerCertificate=yes'
    conn = pyodbc.connect(connectionString)
    return conn
def close_conn(conn):
    conn.close()