from gevent import monkey
monkey.patch_all()

# 是否开启debug模式
debug = True
# 访问地址
bind = "0.0.0.0:8080"
# 设置协程模式
worker_class = "gevent"
# 最大客户端并发数量，默认情况下这个值为1000。此设置将影响gevent和eventlet工作模式
worker_connections = 500
# 超时时间
timeout = 600
# 输出日志级别
loglevel = 'info'
# 存放日志路径
# pidfile = "/var/log/wechat-web/gunicorn.pid"
# 存放日志路径
# accesslog = "/var/log/wechat-web/access.log"
# 设置这个值为true 才会把打印信息记录到错误日志里,将stdout / stderr重定向到errorlog中的指定文件
# capture_output = True
# 存放日志路径
# errorlog = "log /debug.log"
