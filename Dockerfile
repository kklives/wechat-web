FROM python:3.8

WORKDIR /home/app

USER root

RUN sed -i 's/deb.debian.org/mirrors.ustc.edu.cn/g' /etc/apt/sources.list.d/debian.sources \
    && apt-get update \
    # upgrade pip \
    && pip config set global.index-url https://pypi.tuna.tsinghua.edu.cn/simple  \
    && pip install --upgrade pip \
    && mkdir -p /home/app/log \
    # get curl for healthchecks
    && apt-get install curl -y
# copy all the files to the container
COPY . .
# python setup
RUN pip install -r requirements.txt

# define the port number the container should expose
EXPOSE 8080

CMD ["gunicorn","-c","config.py","app:app"]